package main

import (
	"fmt"
	"net/url"

	"github.com/jessevdk/go-flags"
)

var (
	opts struct {
		URL string `short:"u" long:"url" required:"yes" description:"Seminar cesga conference url"`
	}
)

func main() {

	flags.Parse(&opts)

	meetingURL, err := url.ParseRequestURI(opts.URL)
	if err != nil {
		panic(err)
	}

	meetingQuery, ok := meetingURL.Query()["meetingId"]
	if !ok {
		panic("meetingId not found in given url")
	}

	meetingID := meetingQuery[0]

	fmt.Printf("https://seminar.cesga.es/presentation/%s/video/webcams.webm\n", meetingID)

}
